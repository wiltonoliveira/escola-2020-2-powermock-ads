package br.ucsal.bes20192.testequalidade.escola.business;

import java.sql.SQLException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateUtil;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ AlunoDAO.class, DateUtil.class})
public class AlunoBOTest {

	/**
	 * Verificar o calculo da idade.
	 * 
	 * Considerando que o ano atual é 2020, um aluno nascido em 2003 tera 17 anos.
	 * 
	 * Caso de teste
	 * 
	 * # | entrada | saida esperada
	 * 
	 * 1 | ano atual=2020 ; ano de nascimento=2003 | idade=17
	 * @throws SQLException 
	 * 
	 */
	
	@Test
	public void testarCalculoIdadeAluno1() throws SQLException {
		Integer anoAtual = 2020;
		Integer matricula = 100;
		Aluno aluno1 = AlunoBuilder.umAluno().comMatricula(matricula).nascidoEm(2003).build();
	
	
		Integer idadeEsperada = 17;
		
		//Mock do ALunoDAO e DateUtil
		PowerMockito.mockStatic(AlunoDAO.class);
		PowerMockito.when(AlunoDAO.encontrarPorMatricula(matricula)).thenReturn(aluno1);
		
		PowerMockito.mockStatic(DateUtil.class);
		PowerMockito.when(DateUtil.obterAnoAtual()).thenReturn(anoAtual);
		
		Integer idadeAtual = AlunoBO.calcularIdade(matricula);
	
		//Comparar idade esperada com idade atual
		Assert.assertEquals(idadeEsperada, idadeAtual);
		
	}

}
