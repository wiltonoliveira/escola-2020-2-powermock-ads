package br.ucsal.bes20192.testequalidade.escola.business;

import java.sql.SQLException;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateUtil;

public class AlunoBO {

	public static Integer calcularIdade(Integer matricula) throws SQLException {
		Aluno aluno = AlunoDAO.encontrarPorMatricula(matricula);
		return DateUtil.obterAnoAtual() - aluno.getAnoNascimento();
	}

}
